#include <stdlib.h> 
#include <time.h>
#include "Coin.h"

Coin::Coin() {
	srand (time(NULL));
	toss();
}

void Coin::toss() {
	sideUp = rand() % 2 ? HEAD : TAIL;
}

std::string Coin::getSideUp() const {
	return sideUp;
}
