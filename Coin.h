#ifndef COIN_H
#define COIN_H

#include <string>

#define HEAD "head"
#define TAIL "tail"

class Coin {
public:
	Coin();
	std::string getSideUp() const;
	void toss();
private:
	std::string sideUp;
};
#endif//COIN_H
