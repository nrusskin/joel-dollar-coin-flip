#include <iostream>
#include "Coin.h"

int main() {
	Coin		coin;
	std::string	sideUp = coin.getSideUp();
	int		sideHeadCount = 0,
			repeatCount = 20;

	std::cout << "Coin initial side: " << sideUp << std::endl;

	for (int i = 0; i < repeatCount; ++i) {
		coin.toss();
		sideUp = coin.getSideUp(); 
		if ( HEAD == sideUp)
			 ++sideHeadCount;
		std::cout << i + 1 << ": " << sideUp << std::endl;
	}

	std::cout << HEAD << ": " << sideHeadCount << std::endl << 
		TAIL << ": " << repeatCount - sideHeadCount << std::endl;
	return 0;
}
